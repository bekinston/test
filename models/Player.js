import {DataTypes, Sequelize} from 'sequelize';

const sequelize = new Sequelize('test-db','admin','admin',{
    dialect: 'sqlite',
    host:'./db.db'
});

sequelize.sync().then(() => console.log('db is ready'));
// sequelize.sync({force: true});

const Player = sequelize.define('Player', {
    first_name: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    last_name: {
        type: DataTypes.STRING,
        allowNull: false,
    },
});

export default Player;
