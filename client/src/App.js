import React, {useEffect, useState} from 'react';
import axios from 'axios';

function App() {
    const [data, setData] = useState([]);
    const [senddata, setSenddata] = useState(
        {
            first_name:'',
            last_name:''
        }
    )

    const changeHandler = event =>{
        setSenddata({...senddata, [event.target.name]:event.target.value});
    }

    useEffect( ()=>{
         axios.get(`/api/player/all`)
            .then(function (response) {
                console.log(response.data);
                setData(response.data.data);
            })
            .catch(function (error) {
                console.log(error);
            });
    },[])


  return (
    <div className="App">
      <header className="App-header" style={{display:'flex', justifyContent:'center', alignItems:'center', flexDirection:'column'}}>
          <div className='inps' style={{display:'flex', flexDirection:'column', width:300}}>
              <input name='first_name' placeholder='First Name' onChange={changeHandler}/>
              <input name='last_name' placeholder='Last Name' onChange={changeHandler}/>
              <button onClick={
                  ()=>{
                      axios.post('/api/player/create', {...senddata}).then(
                          response=>{console.log(response.data.message); window.location.reload();}
                      ).catch(error=>{
                          console.log(error);
                      })
                  }}>
                  create
              </button>
          </div>

              {
               data.length > 0 &&
                  <>
                  <table>
                      <tr>
                          <th>first name</th>
                          <th>last name</th>
                          <th>number</th>
                      </tr>
                      {
                          data.map((datas, index)=>(

                              <tr key={index}>
                                  <th>{datas.first_name}</th>
                                  <th>{datas.last_name}</th>
                                  <th>{datas.id}</th>
                                  <th><button onClick={
                                      ()=>{
                                          axios.post('/api/player/delete', {id:datas.id}).then(
                                              response=>{console.log(response.data)}
                                          )
                                          window.location.reload();
                                      }
                                  }>delete</button></th>
                              </tr>

                          ))
                      }
                  </table>
                  </>
              }

      </header>
    </div>
  );
}

export default App;
