import express from 'express';
const app = express();
import config from 'config';
import path from 'path';
import http from 'http';
import router from  './routes/player.routes.js'

const PORT = config.get('port') || 8080;

app.use(express.json());

app.use('/api/player', router);

if (process.env.NODE_ENV === 'production') {
    app.use('/', express.static(path.resolve('./', 'client', 'build')))

    app.get('*', (req, res) => {
        res.sendFile(path.resolve('./', 'client', 'build', 'index.html'))
    })
}

async function start() {
    try {
        http.createServer(app).listen(PORT, () => console.log(`App has been started on port ${PORT} ...`));
    } catch (e) {
        console.log('Server Error', e.message);
        process.exit(1);
    }
}

start();
