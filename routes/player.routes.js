import Router from 'express';
import Player from "../models/Player.js";
import {check, validationResult} from "express-validator";

const router = Router();

// //create-player
router.post('/create',
    [
                check('first_name', 'Name cannot be empty').isLength({ min: 2 }),
                check('last_name', 'Last Name cannot be empty').isLength({ min: 2 })
    ],
    async(req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        console.log(errors);
        return res.status(400).json({
            errors: errors.array(),
            message: 'invalid data'
        })
    }
    try {
        const {first_name, last_name} = req.body;
        const user = await Player.build({first_name, last_name});
        await user.save();

        res.status(201).json({"message":"success"})

    }catch (e){
        res.status(500).json({"message":e})
    }
});

//delete-player
router.post("/delete", async (req, res, next) => {
    try {
        const {id} = req.body;
        if(id === 10){
            return res.status(400).json({"message":"cannot delete player id 10"})
        }
        await Player.destroy({ where: { id: id } });
        res.status(201).json({"data": `player ${id} deleted`});
    }catch (e){
        res.status(500).json({"message":e})
    }
});

//get-all-players
router.get("/all", async (req, res, next) => {
    try {
        const allPlayers = await Player.findAll();
        res.status(201).json({"data":allPlayers});
    }catch (e){
        res.status(500).json({"message":e})
    }
});

export default router;


